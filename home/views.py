from django.shortcuts import render, HttpResponse, redirect
from home.models import Contact, Song
from django.contrib import messages 



def home(request): 
    return render(request, "home/home.html")
def mantras(request): 
    allsongs = Song.objects.all()
    context={'allsongs': allsongs}
    return render(request, "home/mantras.html", context)
def books(request): 
    return render(request, "home/books.html")
def quiz(request): 
    return render(request, "home/quiz.html")
def quiz1(request): 
    return render(request, "home/quiz1.html")
def quiz2(request): 
    return render(request, "home/quiz2.html")
def quiz3(request): 
    return render(request, "home/quiz3.html")
def games(request): 
    return render(request, "home/games.html")
def game1(request): 
    return render(request, "home/game1.html")
def game2(request): 
    return render(request, "home/game2.html")
def game3(request): 
    return render(request, "home/game3.html")

def contact(request):
    if request.method=="POST":
        name=request.POST['name']
        email=request.POST['email']
        phone=request.POST['phone']
        content =request.POST['content']
        if len(name)<2 or len(email)<3 or len(phone)<10 or len(content)<4:
            messages.error(request, "Please fill the form correctly")
        else:
            contact=Contact(name=name, email=email, phone=phone, content=content)
            contact.save()
            messages.success(request, "Your message has been successfully sent")
    return render(request, "home/contact.html")
