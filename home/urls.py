from django.contrib import admin
from django.urls import path, include
from home import views

urlpatterns = [
    path('', views.home, name="home"),
    path('mantras', views.mantras, name="mantras"),
    path('books', views.books, name="books"),
    path('quiz', views.quiz, name="quiz"),
    path('quiz1', views.quiz1, name="quiz1"),
    path('quiz2', views.quiz2, name="quiz2"),
    path('quiz3', views.quiz3, name="quiz3"),
    
    path('contact', views.contact, name="contact"),

]

